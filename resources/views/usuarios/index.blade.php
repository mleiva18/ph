@extends("../layouts.starter")

@section("cabecera")

<hr>

@endsection

@section("contenido")

<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">-->

@include('mensajes.success')
@include('mensajes.error')
<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#138496;">LISTADO DE PACIENTES</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example" class="table table-bordered table-hover" style="width:100%"> <!-- table table-striped table-bordered -->
            <thead>
                <tr>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>DNI</th>
                    <th>E-mail</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $usuario)
                    <tr>
                        <td>{{$usuario->name}}</td>
                        <td>{{$usuario->apellido}}</td>
                        <td>{{$usuario->du}}</td>
                        <td>{{$usuario->email}}</td>
                        <td align="center"><a href="{{route('usuarios.show', $usuario->id)}}" class="far fa-eye fa-1x" ><br></td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>DNI</th>
                    <th>E-mail</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>

    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->

@endsection

@section("pie")
PIE
@endsection