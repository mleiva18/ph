@extends("../layouts.starter")

@section("cabecera")



@endsection

@section('contenido')
<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#138496;">PACIENTE</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {!! Form::label('name', 'Nombres: ') !!}    
                </div>

                <div class="col-md-1"></div>
                
                <div class="col-md-2">
                    {!! Form::label('apellido', 'Apellidos: ') !!}
                </div>

                <div class="col-md-1"></div>
                    
                <div class="col-md-2">
                    {!! Form::label('fecha', 'F. de Nacimiento: ') !!}
                </div>

                <div class="col-md-1"></div>
                    
                <div class="col-md-2">
                    {!! Form::label('du', 'N° de DNI:') !!}
                </div>

                
            </div>
            <div class="row">
                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->name}}" readonly>
                </div>
                
                <div class="col-md-1"></div>
                
                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->apellido}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->fecha}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->du}}" readonly>
                </div>  
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    {!! Form::label('domicilio', 'Domicilio: ') !!}    
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    {!! Form::label('nro', 'N°: ') !!}    
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    {!! Form::label('provincia_id', 'Provincia: ') !!}    
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    {!! Form::label('localidade_id', 'Localidad: ') !!}    
                </div>

                <div class="col-md-1"></div>
                
            </div>

            <div class="row">
                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->domicilio}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->nro}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->provincia->NombreProvincia}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->localidade->NombreLocalidad}}" readonly>
                </div>

                <div class="col-md-1"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                {!! Form::label('email', 'E-mail: ') !!}    
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                {!! Form::label('tel', 'Teléfono: ') !!}    
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                {!! Form::label('seguro_id', 'Obra Social: ') !!}    
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                {!! Form::label('nroseguro', 'N° O. Social: ') !!}    
                </div>

                <div class="col-md-1"></div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->email}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->tel}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->seguro->NombreSeguro}}" readonly>
                </div>

                <div class="col-md-1"></div>

                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->nroseguro}}" readonly>
                </div>

                <div class="col-md-1"></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                {!! Form::label('role_id', 'Rol: ') !!}    
                </div>
                <div class="col-md-1"></div>      
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <input type="text" value="{{$usuarios->role->nombre}}" readonly>
                </div>
                <div class="col-md-1"></div>      
            </div>
        </div>

        <br>


        {!! Form::open(['url' => "/usuarios/$usuarios->id", 'method' => 'POST']) !!}
                {{csrf_field()}}
                @include('mensajes.error')
            <div class="row">
                <div class="col-md-9"></div>
                <div class="col-md-1">
                    <a class="btn btn-info" href="{{route('usuarios.edit', $usuarios->id)}}" role="button">Editar</a>
                </div>
                <div class="col-md-1">
                <input type="hidden" name="_method" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('¿Está seguro?')">            
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>



<br>

<br>
@endsection

@section("pie")

@endsection