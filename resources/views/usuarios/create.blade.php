@extends("../layouts.base")

@section("cabecera")

<hr>

@endsection

@section("contenido")

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

{!! Form::open(['url' => "usuarios", 'method' => 'POST']) !!}
{{csrf_field()}}

<div class="col-md-12">
    <div class="row">
        <div class="col-md-2">
            {!! Form::label('name', 'Nombres: ') !!}    
        </div>

        <div class="col-md-1"></div>
        
        <div class="col-md-2">
            {!! Form::label('apellido', 'Apellidos: ') !!}
        </div>

        <div class="col-md-1"></div>
            
        <div class="col-md-2">
            {!! Form::label('fecha', 'F. de Nacimiento: ') !!}
        </div>

        <div class="col-md-1"></div>
            
        <div class="col-md-2">
            {!! Form::label('du', 'N° de DNI:') !!}
        </div>

        
    </div>
    <div class="row">
        <div class="col-md-2">
            {!! Form::text('name') !!}
        </div>
        
        <div class="col-md-1"></div>
        
        <div class="col-md-2">
            {!! Form::text('apellido') !!}
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::date('fecha') !!}
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::text('du') !!}
        </div>
        
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <div class="col-md-2">
            {!! Form::label('domicilio', 'Domicilio: ') !!}    
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::label('nro', 'N°: ') !!}    
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::label('provincia_id', 'Provincia: ') !!}    
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::label('localidade_id', 'Localidad: ') !!}    
        </div>

        <div class="col-md-1"></div>
        
    </div>

    <div class="row">
        <div class="col-md-2">
            {!! Form::text('domicilio') !!}
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::text('nro') !!}
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            <!--{!! Form::select('provincias.id') !!}-->
            <select name="provincia_id" id="">
                <option value="">-- Elija una opción --</option>
                @foreach ($usuarios as $usuario)
                    <option value="{{$usuario->id}}">{{$usuario->NombreProvincia}}</option>
                @endforeach
            </select>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            <!--{!! Form::select('provincias.id') !!}-->
            <select name="localidade_id" id="">
                <option value="">-- Elija una opción --</option>
                @foreach ($localidades as $localidade)
                    <option value="{{$localidade->id}}">{{$localidade->NombreLocalidad}}</option>
                @endforeach
            </select>
        </div>

        <div class="col-md-1"></div>
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <!--<div class="col-md-2">
           {!! Form::label('email', 'E-mail: ') !!}    
        </div>-->

        <div class="col-md-1"></div>

        <div class="col-md-2">
           {!! Form::label('tel', 'Teléfono: ') !!}    
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
           {!! Form::label('seguro_id', 'Obra Social: ') !!}    
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
           {!! Form::label('nroseguro', 'N° O. Social: ') !!}    
        </div>

        <div class="col-md-1"></div>
    </div>

    <div class="row">
       <!-- <div class="col-md-2">
            {!! Form::text('email') !!}
        </div>-->

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::text('tel') !!}
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            <select name="seguro_id" id="">
                <option value="">-- Elija una opción --</option>
                @foreach ($seguros as $seguro)
                    <option value="{{$seguro->id}}">{{$seguro->NombreSeguro}}</option>
                @endforeach
            </select>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-2">
            {!! Form::text('nroseguro') !!}
        </div>

        <div class="col-md-1"></div>
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <div class="col-md-2">
           {!! Form::label('role_id', 'Rol: ') !!}    
        </div>
        <div class="col-md-1"></div>      
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <div class="col-md-2">
          <select name="role_id" id="">
                <option value="">-- Elija una opción --</option>
                @foreach ($roles as $rol)
                    <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                @endforeach
            </select>  
        </div>
        <div class="col-md-1"></div>      
    </div>
</div>

<div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-4">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        
                            <label for="password-confirm" class="col-md-2 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-4">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

<div class="col-md-12">
<div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-1">
        {!! Form::submit('Enviar',['class'=>'btn btn-info'])!!} 
    </div>
    <div class="col-md-1">
        {!! Form::reset('Reset',['class'=>'btn btn-info'])!!}
    </div>
</div>
</div>



<br><br>
    <!--</form>-->
    {!! Form::close() !!}

    @if(count($errors)>0)
    
        @foreach ($errors->all() as $error)
            {{$error}}
        @endforeach

    @endif

@endsection

@section("pie")

@endsection