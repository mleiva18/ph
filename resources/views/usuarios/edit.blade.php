@extends("../layouts.starter")

@section("cabecera")


@endsection

@section("contenido")
<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#138496;">EDITAR PACIENTE</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div id="contenedor" >
            <!--<form method="POST" action="../../usuarios/{{$usuarios->id}}">-->
            {!! Form::open(['url' => "/usuarios/$usuarios->id", 'method' => 'POST']) !!}
            
            @include('mensajes.success')
            {{csrf_field()}}
            <input type="hidden" name="_method" value="PUT">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::label('name', 'Nombres: ') !!}    
                    </div>

                    <div class="col-md-1"></div>
                    
                    <div class="col-md-2">
                        {!! Form::label('apellido', 'Apellidos: ') !!}
                    </div>

                    <div class="col-md-1"></div>
                        
                    <div class="col-md-2">
                        {!! Form::label('fecha', 'F. de Nacimiento: ') !!}
                    </div>

                    <div class="col-md-1"></div>
                        
                    <div class="col-md-2">
                        {!! Form::label('du', 'N° de DNI:') !!}
                    </div>

                    
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::text('name',$usuarios->name) !!}
                    </div>
                    
                    <div class="col-md-1"></div>
                    
                    <div class="col-md-2">
                        {!! Form::text('apellido',$usuarios->apellido) !!}
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::date('fecha',$usuarios->fecha) !!}
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::text('du',$usuarios->du) !!}
                    </div>
                    
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::label('domicilio', 'Domicilio: ') !!}    
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::label('nro', 'N°: ') !!}    
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::label('provincia_id', 'Provincia: ') !!}    
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::label('localidade_id', 'Localidad: ') !!}    
                    </div>

                    <div class="col-md-1"></div>
                    
                </div>

                <div class="row">
                    <div class="col-md-2">
                        {!! Form::text('domicilio',$usuarios->domicilio) !!}
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::text('nro',$usuarios->nro) !!}
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        <!--{!! Form::select('provincias.id') !!}-->
                        <select name="provincia_id" id="">
                            <option value="">-- Elija una opción --</option>
                            @foreach ($provincias as $provincia)
                                <option value="{{$provincia->id}}">{{$provincia->NombreProvincia}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        <!--{!! Form::select('provincias.id') !!}-->
                        <select name="localidade_id" id="">
                            <option value="">-- Elija una opción --</option>
                            @foreach ($localidades as $localidade)
                                <option value="{{$localidade->id}}">{{$localidade->NombreLocalidad}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                    {!! Form::label('email', 'E-mail: ') !!}    
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                    {!! Form::label('tel', 'Teléfono: ') !!}    
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                    {!! Form::label('seguro_id', 'Obra Social: ') !!}    
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                    {!! Form::label('nroseguro', 'N° O. Social: ') !!}    
                    </div>

                    <div class="col-md-1"></div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        {!! Form::text('email',$usuarios->email) !!}
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::text('tel',$usuarios->tel) !!}
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        <select name="seguro_id" id="">
                            <option value="">-- Elija una opción --</option>
                            @foreach ($seguros as $seguro)
                                <option value="{{$seguro->id}}">{{$seguro->NombreSeguro}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-2">
                        {!! Form::text('nroseguro',$usuarios->nroseguro) !!}
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                    {!! Form::label('role_id', 'Rol: ') !!}    
                    </div>
                    <div class="col-md-1"></div>      
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                    <select name="role_id" id="">
                            <option value="">-- Elija una opción --</option>
                            @foreach ($roles as $rol)
                                <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                            @endforeach
                        </select>  
                    </div>
                    <div class="col-md-1"></div>      
                </div>
            </div>   
                
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-1">
                    <input class="btn btn-info" type="submit" name="enviar" value="Actualizar">
                    </div>
                    
                    
                </div>
                

                
            <!--</form>-->
            {!! Form::close() !!}

            <!--<form method="POST" action="../../usuarios/{{$usuarios->id}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="Eliminar registro">

            </form>-->

            



        </div>
    </div>
</div>
@endsection

@section("pie")

@endsection