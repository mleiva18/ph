<!DOCTYPE html>
<html>
<head>
    <title>Lab</title>
    
    <meta charset="utf-8">
  
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/estilos.css')}}">
    <script type="text/javascript" src="Scripts/jquery-2.1.1.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
    
    
</head>
<body>
<div id="contenedor" >    
                    
        
            <header class="blog-header py-3">
                <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4 pt-1">
                    <img src="{{ asset('images/logo.jpg') }}" alt="" width="100px">
                </div>
                <div class="col-4 text-center">
                    <h1 id="inicial">PH</h1>
                    <p style="font-size:20px;">Laboratorio de Análisis Clínicos</p>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                    <a class="text-muted" href="#" aria-label="Search">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img" viewBox="0 0 24 24" focusable="false"><title>Search</title><circle cx="10.5" cy="10.5" r="7.5"/><path d="M21 21l-5.2-5.2"/></svg>
                    </a>
                    <a class="btn btn-sm btn-outline-secondary" href="#">Sign up</a>
                </div>
                </div>
            </header>
            @yield("cabecera")
    
        <div id="centro">
            <div id="barra">
                <div class="row" >
                    <div class="col-lg-1">
                        <!-- BOTON PROFESIONALES -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Profesionales
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ asset('/profesionales') }}">Listar</a>
                                <a class="dropdown-item" href="{{ asset('/profesionales/create') }}">Nuevo</a>
                            </div>
                            </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-1">
                        <!-- BOTON OBRAS SOCIALES -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Obras Sociales
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ asset('/seguros') }}">Listar</a>
                                <a class="dropdown-item" href="{{ asset('/seguros/create') }}">Nueva</a>
                            </div>
                            </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-1">
                        <!-- BOTON USUARIOS -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Pacientes
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ asset('/usuarios') }}">Listar</a>
                                <a class="dropdown-item" href="{{ asset('/usuarios/create') }}">Nuevo</a>
                            </div>
                            </div>
                    </div>
                </div>
                <!--<table>
                    <tr>
                        <td>
                            
                            <div class="btn-group">adf
                            <button type="button" class="btn btn-info dropdown-toggle custom" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Profesionales
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{ asset('/profesionales') }}">Listar</a>
                                <a class="dropdown-item" href="{{ asset('/profesionales/create') }}">Nuevo</a>
                            </div>
                            </div>
                        </td>
                    </tr><tr><td><br></td></tr>
                    <tr>
                        <td>
                            
                        
                        </td>
                    </tr>
                </table>-->
            </div>
            <div id="cont">
                @yield("contenido")
            </div>
            
        </div>
        
        <div id="pie">
            @yield("pie")
            <footer class="blog-footer">
                <p>Martín Leiva editions</p>
                <p>
                    <a href="#">Mandame un mail.</a>
                </p>
            </footer>
        </div>
    </div>   
    
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>