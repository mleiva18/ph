<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>PH - Laboratorio</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/blog/">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/estilos.css')}}">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <!-- Custom styles for this template 
    <link href="blog.css" rel="stylesheet">-->
  </head>
  <body>
    <div class="container" >
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand  navbar-light bg-custom" style="border-radius:10px;">
       
      <div class="col-md-2">
        <img src="{{ asset('images/bco.png') }}" alt="" height="35em">
      </div>
      <div class="col-md-8">
        <img src="{{ asset('images/nombrebco.png') }}" alt="" height="35em" style="margin-left:auto;margin-right:auto;display:block">
      </div>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
        
        <ul class="navbar-nav ml-auto">
          <!-- Authentication Links -->
          @guest
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              @if (Route::has('register'))
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                  </li>
              @endif
          @else
              <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="navbar-brand dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color:white">
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>
          @endguest
        </ul>
          
        </ul>
      </nav>
      <!-- /.navbar -->
        <!--<div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
            <div class="col-md-auto px-0">
            <h1 class="display-4 font-italic">PH</h1>
            </div>
        </div>-->

        <br>
        <div class="form-group row">
          <div class="col-md-12">
            <!--<img class="img-fluid" src="{{ asset('images/banner/banner3.png') }}" alt="" width="100%" style="border-radius: 10px;">-->
            <img src="" id="banner" width="100%" style="border-radius: 10px;">
          </div>
        </div>

       
    </div>

      @yield('contenido')

  <div class="container" >
    <footer class="main-footer" style="border:solid 1px;border-color:#008aad;border-radius:15px 5px">
      <div class="col-md-12">
        Todos los derechos reservados.-
          <div class="float-right d-none d-sm-inline" >
            Desarrollo ML
          </div>
      </div>
    </footer>
  </div>

<script type="text/javascript" language="javascript" src="{{asset('js/banner.js')}}"></script>

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>

<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>

<!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>-->
<script type="text/javascript" language="javascript" src="{{asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>

<!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>-->
<script type="text/javascript" language="javascript" src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript" class="init">



</body>
</html>
