<!DOCTYPE html>
<html>
<head>
	<title>Lab</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    
    <style>
        .logo{
            float:right;
            padding-right: 100px;
            width:100px;
        }

        .cabecera{
            text-align:center;
            font-size:x-large;
            font-family:verdana;
            margin-bottom:100px;
            color:blue;
        }

        .contenido form, li{
            width:300px;
            margin:0 auto;
        }

        .pie{
            position:fixed;
            bottom:0px;
            width:100%;
            font-size:0.7em;
            margin-bottom:15px;
        }
    </style>
</head>
<body>
    <div class="col-md-12" >                
        <div class="col-md-2 col-md-offset-3" style="border: solid 1px; border-color:#3c8dbc;border-radius:8px">
            <div  style="max-width: 100px;">
                <img src="../../public/images/logo.jpg" class="card-img-center">
            </div>
        </div>
        @yield("cabecera")
    </div>
        
    

    <div class="contenido">
        @yield("contenido")
    </div>

    <div class="pie">
        @yield("pie")
        Química Clínica - Hematología - Parasitología - Bacteriología <br>
        Est. Hormonales - Marcadores Tumorales - Est. Biomoleculares
    </div>


    
</body>
</html>