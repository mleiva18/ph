@extends('layouts.starter')

@section('contenido')
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><!--{{ __('Register') }}--><h3 class="card-title" style="color:#008aad;">AGREGAR PACIENTE</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">Nombres<!--{{ __('Name') }}--></label>

                            <div class="col-md-4">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <label apellido="apellido" class="col-md-2 col-form-label text-md-right" for="apellido">Apellidos</label>

                            <div class="col-md-4">
                                <input type="text" id="apellido" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ old('apellido') }}" required autocomplete="apellido" autofocus>

                                @error('apellido')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            </div>
                        </div>

                        <div class="form-group row">
                                <div class="col-md-2 col-form-label text-md-right">
                                    {!! Form::label('fecha', 'F. de Nacimiento ') !!}
                                </div>

                                <div class="col-md-4">
                                    <!--{!! Form::date('fecha',null, array('class' => 'form-control')) !!}-->
                                    <input type="date" id="fecha" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ old('fecha') }}" required autocomplete="fecha" autofocus>

                                        @error('fecha')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>

                                

                                <div class="col-md-2 col-form-label text-md-right">
                                    {!! Form::label('du', 'N° de DNI') !!}
                                </div>

                                <div class="col-md-4">
                                    <!--{!! Form::text('du',null, array('class' => 'form-control')) !!}-->
                                    <input type="text" id="du" class="form-control @error('du') is-invalid @enderror" name="du" value="{{ old('du') }}" required autocomplete="du" autofocus>

                                        @error('du')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 text-md-right">
                                {!! Form::label('domicilio', 'Domicilio ') !!}    
                            </div>

                            <div class="col-md-4 ">
                                <!--{!! Form::text('domicilio',null, array('class' => 'form-control')) !!}-->
                                <input type="text" id="domicilio" class="form-control @error('domicilio') is-invalid @enderror" name="domicilio" value="{{ old('domicilio') }}" required autocomplete="domicilio" autofocus>

                                    @error('domicilio')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="col-md-2 text-md-right">
                                {!! Form::label('nro', 'Nro ') !!}    
                            </div>

                            <div class="col-md-2">
                                <!--{!! Form::text('nro',null, array('class' => 'form-control')) !!}-->
                                <input type="text" id="nro" class="form-control @error('nro') is-invalid @enderror" name="nro" value="{{ old('nro') }}" required autocomplete="nro" autofocus>

                                    @error('nro')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 text-md-right">
                                {!! Form::label('provincia_id', 'Provincia ') !!}    
                            </div>

                            <div class="col-md-4">
                                <select id="provincia_id" class="form-control @error('provincia_id') is-invalid @enderror" name="provincia_id" value="{{ old('provincia_id') }}" required autocomplete="provincia_id" autofocus>
                                    <option value="">-- Elija una opción --</option>
                                    @foreach ($usuarios as $usuario)
                                        <option value="{{$usuario->id}}">{{$usuario->NombreProvincia}}</option>
                                    @endforeach
                                </select>
                                    @error('provincia_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="col-md-2 text-md-right">
                                {!! Form::label('localidade_id', 'Localidad ') !!}    
                            </div>

                            <div class="col-md-4">
                                <select id="localidade_id" class="form-control @error('localidade_id') is-invalid @enderror" name="localidade_id" value="{{ old('localidade_id') }}" required autocomplete="localidade_id" autofocus>
                                    <option value="">-- Elija una opción --</option>
                                    @foreach ($localidades as $localidade)
                                        <option value="{{$localidade->id}}">{{$localidade->NombreLocalidad}}</option>
                                    @endforeach
                                </select>
                                    @error('localidade_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 text-md-right">
                                {!! Form::label('tel', 'Teléfono ') !!}    
                            </div>

                            <div class="col-md-4">
                                <!--{!! Form::text('tel',null, array('class' => 'form-control')) !!}-->
                                <input type="text" id="tel" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}" required autocomplete="tel" autofocus>

                                    @error('tel')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="col-md-2 text-md-right">
                                {!! Form::label('role_id', 'Rol ') !!}    
                            </div>                       
                            
                            <div class="col-md-4">
                                <select id="role_id" class="form-control @error('role_id') is-invalid @enderror" name="role_id" value="{{ old('role_id') }}" required autocomplete="role_id" autofocus>
                                    <option value="">-- Elija una opción --</option>
                                    @foreach ($roles as $rol)
                                        <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                                    @endforeach
                                </select>  
                                @error('role_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-2 text-md-right">
                                {!! Form::label('seguro_id', 'Obra Social ') !!}    
                            </div>

                            <div class="col-md-4">
                                <select id="seguro_id" class="form-control @error('seguro_id') is-invalid @enderror" name="seguro_id" value="{{ old('seguro_id') }}" required autocomplete="seguro_id" autofocus>
                                    <option value="">-- Elija una opción --</option>
                                    @foreach ($seguros as $seguro)
                                        <option value="{{$seguro->id}}">{{$seguro->NombreSeguro}}</option>
                                    @endforeach
                                </select>
                                    @error('seguro_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                            <div class="col-md-2 text-md-right">
                                {!! Form::label('nroseguro', 'N° Obra Social ') !!}    
                            </div>

                            <div class="col-md-4">
                                <!--{!! Form::text('nroseguro',null, array('class' => 'form-control')) !!}-->
                                <input type="text" id="nroseguro" class="form-control @error('nroseguro') is-invalid @enderror" name="nroseguro" value="{{ old('nroseguro') }}" required autocomplete="nroseguro" autofocus>

                                    @error('nroseguro')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>

                        </div>

                       
                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-4">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-2 text-md-right">
                                {!! Form::label('username', 'Nombre de Usuario') !!}    
                            </div>                       
                            
                            <div class="col-md-4">
                                <input type="text" id="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}"  autocomplete="username" autofocus>

                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>


                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-4">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        
                            <label for="password-confirm" class="col-md-2 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-4">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-info">
                                    Guardar<!--{{ __('Register') }}-->
                                </button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
