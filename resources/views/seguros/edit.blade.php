@extends("../layouts.base")

@section("cabecera")

<hr>

@endsection

@section("contenido")
<div id="contenedor" >
    <!--<form method="POST" action="../../seguros/{{$seguros->id}}">-->
    {!! Form::open(['url' => "/seguros/$seguros->id", 'method' => 'POST']) !!}

    @include('mensajes.success')
        <table>
            <tr>
                
                {{csrf_field()}}
                
                <input type="hidden" name="_method" value="PUT">
                <td>{!! Form::label('NombreSeguro', 'Compañía:') !!}</td>
                <td>{!! Form::text('NombreSeguro',$seguros->NombreSeguro) !!}</td>
                
            </tr>
           
            <!--<tr>
                <td><input type="submit" name="enviar" value="Actualizar"></td>
                <td><input type="reset" name="Borrar" value="Limpiar"></td>
            </tr>-->
        </table>
        
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-1">
            <input class="btn btn-info" type="submit" name="enviar" value="Actualizar">
            </div>
            
            
        </div>
        

        
    <!--</form>-->
    {!! Form::close() !!}

    <!--<form method="POST" action="../../seguros/{{$seguros->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" value="Eliminar registro">

    </form>-->

    



</div>
@endsection

@section("pie")

@endsection