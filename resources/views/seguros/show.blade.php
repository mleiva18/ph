@extends("../layouts.base")

@section("cabecera")

<hr>

@endsection

@section("contenido")



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <label>Nombres: </label><br>
                <input type="text" value=" {{$seguros->NombreSeguro}} " readonly>
            </div>
            
            
            
        </div>
    </div>
</div>
<br>


{!! Form::open(['url' => "/seguros/$seguros->id", 'method' => 'POST']) !!}
        {{csrf_field()}}
        @include('mensajes.error')
    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-1">
            <a class="btn btn-info" href="{{route('seguros.edit', $seguros->id)}}" role="button">Editar</a>
        </div>
        <div class="col-md-1">
           <input type="hidden" name="_method" value="DELETE">
            <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('¿Está seguro?')">            
        </div>
    </div>
{!! Form::close() !!}

<br>
@endsection

@section("pie")

@endsection