@extends("../layouts.base")

@section("cabecera")

<hr>

@endsection

@section("contenido")

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">


    <!--<form method="POST" action="../afiliados">-->

    {!! Form::open(['url' => "seguros", 'method' => 'POST']) !!}
        <table>
            <tr>
                <td>{!! Form::label('NombreSeguro', 'Compañía') !!}</td>
                <td>{!! Form::text('NombreSeguro') !!}</td>
                
                {{csrf_field()}}
            </tr>
            
            

            <tr>
                <!--<td>{!! Form::submit('Enviar',['class'=>'btn btn-info'])!!}</td>
                <td>{!! Form::reset('Reset',['class'=>'btn btn-info'])!!}</td>
                <td><input type="submit" name="enviar" value="Enviar"></td>
                <td><input type="reset" name="Borrar" value="Limpiar"></td>-->
                
            </tr>
        </table>

        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-1">
                {!! Form::submit('Enviar',['class'=>'btn btn-info'])!!} 
            </div>
            <div class="col-md-1">
                {!! Form::reset('Reset',['class'=>'btn btn-info'])!!}
            </div>
        </div>
        <br><br>
    <!--</form>-->
    {!! Form::close() !!}

    @if(count($errors)>0)
    
        @foreach ($errors->all() as $error)
            {{$error}}
        @endforeach

    @endif

@endsection

@section("pie")

@endsection