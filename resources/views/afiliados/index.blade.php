@extends("../layouts.plantilla")

@section("cabecera")

LLER REGISTROS

@endsection

@section("contenido")

@include('mensajes.success')

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabla Contratados</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dt_cliente" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nombres</th>
                      <th>Apellidos</th>
                      <th>DNI</th>
                      <th>Domicilio</th>
                      <th>Nro</th>
                      <th>Pcia</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>DNI</th>
                        <th>Domicilio</th>
                        <th>Nro</th>
                        <th>Pcia</th>
                        <th></th>
                        
                    </tr>
                  </tfoot>
                  <tbody>
                    
                  @foreach($users as $afiliado)
                    <tr>
                        <td>{{$afiliado->nombres}}</td>
                        <td>{{$afiliado->apellidos}}</td>
                        <td>{{$afiliado->du}}</td>
                        <td>{{$afiliado->domicilio}}</td>
                        <td>{{$afiliado->nro}}</td>
                        <td>{{$afiliado->NombreProvincia}}</td>
                        <td align="center">
                        <a href="{{route('afiliados.edit', $afiliado->id)}}" class="far fa-eye fa-1x" ><br>
                          Ver
                        </a>
                      </td>
                      <td><a download href="images/{{$afiliado->ruta}}" target="_blank">Descargar</a></td>
                      
                    </tr>@endforeach

                    

                  </tbody>
                </table><a href="afiliados/create">NUEVO</a><BR><BR></BR>
              </div>
            </div>
          </div>








@endsection

@section("pie")

@endsection