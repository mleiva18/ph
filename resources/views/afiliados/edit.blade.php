@extends("../layouts.plantilla")

@section("cabecera")

editar REGISTROS

@endsection

@section("contenido")

    <form method="POST" action="../../afiliados/{{$afiliados->id}}">

        <table>
            <tr>
                <td>Nombres:</td>
                <td><input type="text" name="nombres" value="{{$afiliados->nombres}}"></td>
                {{csrf_field()}}
                
                <input type="hidden" name="_method" value="PUT">

                
                


            </tr>
            <tr>
                <td>Apellidos:</td>
                <td><input type="text" name="apellidos" value="{{$afiliados->apellidos}}"></td>
                
            </tr>
            <tr>
                <td>Dirección:</td>
                <td><input type="text" name="domicilio" value="{{$afiliados->domicilio}}"></td>
                
            </tr>
            <tr>
                <td>N°:</td>
                <td><input type="text" name="nro" value="{{$afiliados->nro}}"></td>
                
            </tr>

            <tr>
                <td><input type="submit" name="enviar" value="Actualizar"></td>
                <td><input type="reset" name="Borrar" value="Borrar campos"></td>
            </tr>
        </table>
        

        

        
    </form>

    <form method="POST" action="../../afiliados/{{$afiliados->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" value="Eliminar registro">

    </form>

@endsection

@section("pie")

@endsection