@extends("../layouts.plantilla")

@section("cabecera")

INSERTAR REGISTROS

@endsection

@section("contenido")

    <!--<form method="POST" action="../afiliados">-->

    {!! Form::open(['url' => "afiliados", 'method' => 'POST', 'files' => true]) !!}

       <table>
            <tr>
                <td>{!! Form::file('file') !!}</td>
            </tr>
       </table>

        <table>
            <tr>
                <td>Nombres:</td>
                <td><input type="text" name="nombres"></td>
                {{csrf_field()}}
            </tr>
            <tr>
                <td>Apellidos:</td>
                <td><input type="text" name="apellidos"></td>
                
            </tr>
            <tr>
                <td>Dirección:</td>
                <td><input type="text" name="domicilio"></td>
                {{csrf_field()}}
            </tr>
            <tr>
                <td>N°:</td>
                <td><input type="text" name="nro"></td>
                {{csrf_field()}}
            </tr>

            <tr>
                <td>Fecha:</td>
                <td><input type="date" name="fecha_nacimiento"></td>
                {{csrf_field()}}
            </tr>

            <tr>
                <td><input type="submit" name="enviar" value="Enviar"></td>
                <td><input type="reset" name="Borrar" value="Borrar"></td>
                <td><a href="../afiliados">volver</a><BR><BR></BR></td>
            </tr>
        </table>
        
    <!--</form>-->
    {!! Form::close() !!}

    @if(count($errors)>0)
    
        @foreach ($errors->all() as $error)
            {{$error}}
        @endforeach

    @endif

@endsection

@section("pie")

@endsection