@extends("../layouts.starter")

@section("cabecera")

@endsection

@section("contenido")

<!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">-->
<!--<form method="POST" action="../afiliados">-->

<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#008aad;">AGREGAR PROFESIONAL</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        {!! Form::open(['url' => "profesionales", 'method' => 'POST', 'files' => true]) !!}
            {{csrf_field()}}
            
            <div class="form-group row">
                <div class="col-md-2 text-md-right">
                    {!! Form::label('NombreProf', 'Nombres') !!}
                </div>

                <div class="col-md-4">
                    <!--{!! Form::text('NombreProf',null,array('class'=>'form-control')) !!}-->
                    <input id="NombreProf" type="text" class="form-control @error('NombreProf') is-invalid @enderror" name="NombreProf" value="{{ old('NombreProf') }}" required autocomplete="NombreProf" autofocus>

                        @error('NombreProf')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="col-md-2 text-md-right">
                    {!! Form::label('ApellidoProf', 'Apellidos') !!}
                </div>

                <div class="col-md-4">
                    <!--{!! Form::text('ApellidoProf',null,array('class'=>'form-control')) !!}-->
                    <input id="ApellidoProf" type="text" class="form-control @error('ApellidoProf') is-invalid @enderror" name="ApellidoProf" value="{{ old('ApellidoProf') }}" required autocomplete="ApellidoProf" autofocus>

                        @error('ApellidoProf')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2 text-md-right">
                    {!! Form::label('correo', 'Correo electrónico') !!}
                </div>

                <div class="col-md-4">
                    <!--{!! Form::text('correo',null,array('class'=>'form-control')) !!}-->
                    <input id="correo" type="text" class="form-control @error('correo') is-invalid @enderror" name="correo" value="{{ old('correo') }}" required autocomplete="correo" autofocus>

                        @error('correo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="col-md-2 text-md-right">
                    {!! Form::label('comentarios', 'Comentarios') !!}
                </div>
                
                <div class="col-md-4">
                    {!! Form::textarea('comentarios',null,array('class'=>'form-control')) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-10"></div>
                <div class="col-md-1">
                    {!! Form::submit('Enviar',['class'=>'btn btn-info'])!!} 
                </div>
                <div class="col-md-1">
                    {!! Form::reset('Reset',['class'=>'btn btn-info'])!!}
                </div>
            </div>
            
        <!--</form>-->
        {!! Form::close() !!}

        @if(count($errors)>0)
        
            @foreach ($errors->all() as $error)
                {{$error}}
            @endforeach

        @endif
    </div>
</div>

@endsection

@section("pie")

@endsection