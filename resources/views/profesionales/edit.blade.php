@extends("../layouts.starter")

@section("cabecera")

@endsection

@section("contenido")

<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#138496;">PROFESIONAL</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div id="contenedor" >
            {!! Form::open(['url' => "/profesionales/$profesionales->id", 'method' => 'POST']) !!}
            @include('mensajes.success')
            <table>
                <tr>
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PUT">
                    <td>{!! Form::label('NombreProf', 'Nombres') !!}</td>
                    <td>{!! Form::text('NombreProf',$profesionales->NombreProf) !!}</td>
                    
                </tr>
                <tr>
                    <td>{!! Form::label('ApellidoProf', 'Apellidos') !!}</td>
                    <td>{!! Form::text('ApellidoProf',$profesionales->ApellidoProf) !!}</td>
                </tr>
                <tr>
                    <td>{!! Form::label('comentarios', 'Comentarios') !!}</td>
                    <td colspan="6" width="100%">{!! Form::textarea('comentarios',$profesionales->comentarios) !!}</td>
                    
                </tr>
                <!--<tr>
                    <td><input type="submit" name="enviar" value="Actualizar"></td>
                    <td><input type="reset" name="Borrar" value="Limpiar"></td>
                </tr>-->
            </table>
            
            <div class="row">
                <div class="col-md-9"></div>
                    <div class="col-md-1">
                        <input class="btn btn-info" type="submit" name="enviar" value="Actualizar">
                    </div>  
            </div>
            {!! Form::close() !!}
            <!--<form method="POST" action="../../profesionales/{{$profesionales->id}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="Eliminar registro">

            </form>-->
        </div>
    </div>
</div>
@endsection

@section("pie")

@endsection