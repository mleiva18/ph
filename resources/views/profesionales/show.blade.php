@extends("../layouts.starter")

@section("cabecera")

<hr>

@endsection

@section("contenido")
<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#138496;">PROFESIONAL</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label>Nombres: </label><br>
                        <input type="text" value=" {{$profesionales->NombreProf}} " readonly>
                    </div>
                
                    <div class="col-md-2">
                        <label>Apellidos: </label><br>
                        <input type="text" value=" {{$profesionales->ApellidoProf}} " readonly>
                    </div>

                    <div class="col-md-auto">
                        <label>Comentarios: </label><br>
                        <input type="text" value=" {{$profesionales->comentarios}} " readonly>
                    </div>
                </div>
            </div>
        </div>
    


        {!! Form::open(['url' => "/profesionales/$profesionales->id", 'method' => 'POST']) !!}
        {{csrf_field()}}
        @include('mensajes.error')
            <div class="row">
                <div class="col-md-9"></div>
                <div class="col-md-1">
                    <a class="btn btn-info" href="{{route('profesionales.edit', $profesionales->id)}}" role="button">Editar</a>
                </div>
                <div class="col-md-1">
                    <input type="hidden" name="_method" value="DELETE">
                    <input class="btn btn-danger" type="submit" value="Eliminar" onclick="return confirm('¿Está seguro?')">            
                </div>
            </div>
        {!! Form::close() !!}

    </div>
</div>
<br>

<br>
@endsection

@section("pie")

@endsection