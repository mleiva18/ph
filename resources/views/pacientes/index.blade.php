@extends('../layouts.pacientes')

@section('contenido')
<div class="container">



    <div class="row mb-2">
            <div class="col-md-12">
               
                        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-200 position-relative">
                            <div class="col-lg-10">
                                <div class="row"> 
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>Nombre</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->name}}
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>Apellido</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->apellido}}
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>DNI</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->du}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>Dirección</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->domicilio}}
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>N°</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->nro}}
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>Provincia</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->provincia->NombreProvincia}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>F. Nac.</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->fecha}}
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>Email</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->email}}
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>Tel</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->tel}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>O.Social</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->seguro->NombreSeguro}}
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-right">
                                        <b>N° O. Social</b> 
                                    </div>
                                    <div class="col-md-2 col-form-label text-md-left">
                                        {{$user->nroseguro}}
                                    </div>
                                </div>
                            </div>
                        
                </div>
            </div>
        </div>
            
        

        <table id="example" class="table table-bordered table-hover" style="width:100%"> <!-- table table-striped table-bordered -->
            <thead>
                <tr>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Fecha</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Paciente</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Médico</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Comentario</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Descargar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $usuario)
                    <tr>
                        <td>{{$usuario->fecha_est}}</td>
                        <td>{{$usuario->apellido}} {{$usuario->name}}</td>
                        <td>{{$usuario->ApellidoProf}} {{$usuario->NombreProf}}</td>
                        <td>{{$usuario->comentarios_est}}</td>
                        <td align="center"><a href="images/resultados/{{$usuario->ruta}}" download> <i class="fas fa-arrow-alt-circle-down fa-2x" style="color: #008aad;"></i></a><br></td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Fecha</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Paciente</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Médico</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Comentario</th>
                    <th bgcolor="#6d6e71" style="color:white;text-align:center;font-family:verdana;">Descargar</th>
                </tr>
            </tfoot>
        </table>
</div>
@endsection
