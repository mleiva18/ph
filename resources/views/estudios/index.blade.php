@extends("../layouts.starter")

@section("cabecera")

<hr>

@endsection

@section("contenido")

<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">-->

@include('mensajes.success')
@include('mensajes.error')
<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#138496;">ESTUDIOS</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example" class="table table-bordered table-hover" style="width:100%"> <!-- table table-striped table-bordered -->
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>N° Orden</th>
                    <th>Paciente</th>
                    <th>Profesional</th>
                    <th>Comentario</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $usuario)
                    <tr>
                        <td>{{$usuario->fecha_est}}</td>
                        <td>{{$usuario->orden}}</td>
                        <td>{{$usuario->apellido}} {{$usuario->name}}</td>
                        <td>{{$usuario->ApellidoProf}} {{$usuario->NombreProf}}</td>
                        <td>{{$usuario->comentarios_est}}</td>
                        <td align="center"><a href="images/resultados/{{$usuario->ruta}}" download> <i class="fas fa-arrow-alt-circle-down fa-2x" style="color: red;"></i></a><br></td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Fecha</th>
                    <th>N° Orden</th>
                    <th>Paciente</th>
                    <th>Profesional</th>
                    <th>Comentario</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>

    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->

@endsection

@section("pie")
PIE
@endsection