@extends("../layouts.starter")

@section("cabecera")

<hr>

@endsection

@section("contenido")

<!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">-->

<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title" style="color:#008aad;">CARGA DE RESULTADOS</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        {!! Form::open(['url' => "estudios", 'method' => 'POST', 'files' => true]) !!}
        {{csrf_field()}}

        <div class="form-group row">
            <div class="col-md-2 text-md-right">
                {!! Form::label('name', 'Paciente ') !!}    
            </div>

            <div class="col-md-4">
                <!--{!! Form::select('provincias.id') !!}-->
                <!--<select name="usuario_id" id="" class="form-control">-->
                <select id="usuario_id" class="form-control @error('usuario_id') is-invalid @enderror" name="usuario_id" value="{{ old('usuario_id') }}" required autocomplete="usuario_id" autofocus>
                    <option value="">-- Elija una opción --</option>
                    @foreach ($usuarios as $usuario)
                        <option value="{{$usuario->id}}">{{$usuario->apellido}} {{$usuario->name}}</option>
                    @endforeach
                </select>
                @error('usuario_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="col-md-2 text-md-right">
                {!! Form::label('ApellidoProf', 'Profesional ') !!}
            </div>

            <div class="col-md-4">
                <!--<select name="profesionals_id" id="" class="form-control">-->
                <select id="profesionals_id" class="form-control @error('profesionals_id') is-invalid @enderror" name="profesionals_id" value="{{ old('profesionals_id') }}" required autocomplete="profesionals_id" autofocus>
                    <option value="">-- Elija una opción --</option>
                    @foreach ($profesionales as $profesionale)
                        <option value="{{$profesionale->id}}">{{$profesionale->ApellidoProf}} {{$profesionale->NombreProf}}</option>
                    @endforeach
                </select>
                @error('profesionals_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

        </div>

        <div class="form-group row">
            <div class="col-md-2 text-md-right">
                {!! Form::label('fecha_est', 'Fecha ') !!}    
            </div>

            <div class="col-md-4">
                <!--{!! Form::date('fecha_est',null,array('class'=>'form-control')) !!}-->
                <input id="fecha_est" type="date" class="form-control @error('fecha_est') is-invalid @enderror" name="fecha_est" value="{{ old('fecha_est') }}" required autocomplete="fecha_est">

                    @error('fecha_est')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror    
            </div>

            <div class="col-md-2 text-md-right">
                {!! Form::label('orden', 'N° Orden') !!}    
            </div>

            <div class="col-md-4">
                <!--{!! Form::text('orden',null,array('class'=>'form-control require')) !!}-->    
                <input id="orden" type="text" class="form-control @error('orden') is-invalid @enderror" name="orden" value="{{ old('orden') }}" required autocomplete="orden">

                    @error('orden')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror  
            </div>

            
        </div>

        <div class="form-group row">

            <div class="col-md-2 text-md-right">
                {!! Form::label('file', 'Subir ') !!}    
            </div>

            <div class="col-md-4">
                <!--{!! Form::file('file') !!}-->
                <input id="file" type="file" class="@error('file') is-invalid @enderror" name="file" value="{{ old('file') }}" required autocomplete="file">
                @error('file')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror  
            </div>

        </div>

        <div class="form-group row">
            
            <div class="col-md-2 text-md-right">
                {!! Form::label('comentarios_est', 'Comentarios ') !!}    
            </div> 

            

            <div class="col-md-10">
                {!! Form::textarea('comentarios_est',null,array('class'=>'form-control')) !!}    
            </div>
            
        </div>



        <div class="col-md-12">
        <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-1">
                {!! Form::submit('Enviar',['class'=>'btn btn-info'])!!} 
            </div>
            <div class="col-md-1">
                {!! Form::reset('Reset',['class'=>'btn btn-info'])!!}
            </div>
        </div>
        </div>



        <br><br>
            <!--</form>-->
            {!! Form::close() !!}

            @if(count($errors)>0)
            
                @foreach ($errors->all() as $error)
                    {{$error}}
                @endforeach

            @endif
        </div>
</div>
@endsection

@section("pie")

@endsection