@extends('../layouts.starter')

@section('contenido')
<br>
<div class="card">
    <div class="card-header">Dashboard</div>

    <!--<div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        You are logged in!
    </div>-->  

<!-- Info boxes --> 
<div class="container-fluid"><br>
    <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="nav-icon fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Pacientes</span>
                <span class="info-box-number">
                {{$cant1}}
                  <small></small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-chart-line"><br>T</i></span>

              <div class="info-box-content">
                <span class="info-box-text">Cantidad Estudios</span>
                <span class="info-box-number">{{$tot}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-chart-line"><br>M</i></span>

              <div class="info-box-content">
                <span class="info-box-text">Cantidad Estudios<br> Mensuales</span>
                <span class="info-box-number">{{$tot1}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-chart-line"><br>D</i></span>

              <div class="info-box-content">
                <span class="info-box-text">Cantidad Estudios<br> Diario</span>
                <span class="info-box-number">{{$tot2}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
</div>
@endsection
