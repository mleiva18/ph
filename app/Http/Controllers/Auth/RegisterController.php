<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Provincia;
use App\Localidade;
use App\Role;
use App\Seguro;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/usuarios'; //RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showRegistrationForm()
    {
        $usuarios = Provincia::all();
        $localidades= Localidade::all();
        $seguros= Seguro::all();
        $roles= Role::all();
        return view('auth.register',compact("usuarios","localidades","seguros","roles"));
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        /*$this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
                    ? new Response('', 201)
                    : redirect($this->redirectPath());*/
                    return redirect('/usuarios')->with('success','Paciente agregado.');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            
            'apellido' => ['required', 'string', 'max:255'],
            'fecha' => ['required', 'date'],
            'du' => ['required', 'unique:users'],
            'domicilio' => ['required', 'string', 'max:255'],
            'nro' => ['required', 'string', 'max:255'],
            'provincia_id' => ['required'],
            'localidade_id' => ['required'],
            'tel' => ['required', 'string', 'max:255'],
            'seguro_id' => ['required'],
            'nroseguro' => ['required', 'string', 'max:255'],
            'role_id' => ['required'],            
            'email' => ['unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'username' => ['string','max:15','unique:users'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //$usuarios = Provincia::all();

        return User::create([
            'name' => $data['name'],
            'apellido' => $data['apellido'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'du' => $data['du'],
            'domicilio' => $data['domicilio'],
            'nro' => $data['nro'],
            'tel' => $data['tel'],
            'fecha' => $data['fecha'],
            'role_id' => $data['role_id'],
            'seguro_id' => $data['seguro_id'],
            'provincia_id' => $data['provincia_id'],
            'localidade_id' => $data['localidade_id'],
            'nroseguro' => $data['nroseguro'],
            'username' => $data['username'],
        ]);

        

        
    }
}
