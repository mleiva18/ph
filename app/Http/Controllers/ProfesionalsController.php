<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Profesional;

class ProfesionalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $profesionales=Profesional::all();
        
        return view("profesionales.index",compact("profesionales"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("profesionales.create");

        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $entrada=$request->all();

        Profesional::create($entrada);

        return redirect('/profesionales')->with('success','Profesional agregado.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $profesionales=Profesional::findOrFail($id);

        return view("profesionales.show", compact("profesionales"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $profesionales=Profesional::findOrFail($id);

        return view ("profesionales.edit", compact("profesionales"));
//        return redirect('/profesionales')->with('success','Profesional modificado.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $profesionales=Profesional::findOrFail($id);

        $profesionales->update($request->all());

        //return redirect("/profesionales");
        return redirect('/profesionales')->with('success','Profesional modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $profesionales=Profesional::findOrFail($id);

        $profesionales->delete();

        //return redirect("/afiliados");
        return redirect('/profesionales')->with('error','Profesional eliminado.');
    }

    
}
