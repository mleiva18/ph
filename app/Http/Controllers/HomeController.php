<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Estudio;
use App\Seguro;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->role_id == 1){

            $cant1= User::where('role_id','=','2')->count();

            //Total estudios mensuales.

            $mesactual=date("m");
            
            $tot1=DB::table('estudios')
            ->whereMonth('created_at',$mesactual)
            ->count();

            //Total estudios.

            $tot=DB::table('estudios')
            ->whereNull('deleted_at')
            ->count();

            //Total estudios diarios.
            $hoy=date("Y-m-d");
            $tot2=DB::table('estudios')
            ->whereDate('created_at','=',now()->format('Y-m-d'))
            ->count();

            return view('/home',compact('cant1','tot1','tot','tot2'));
        }
    
            if (auth()->user()->role_id == 2){
                /*$id=3;//auth()->user()->id;
                $usuarios = DB::table('estudios')
                ->join('users','users.id','=','estudios.usuario_id')
                ->join('profesionals','profesionals.id','=','estudios.profesionals_id')
                ->select('users.*','estudios.*','profesionals.*')
                ->where('estudios.usuario_id','=',$id)
                ->get();*/

                $id=auth()->user()->id;
                $user=User::find($id);
                $usuarios= DB::table('users')
                ->join('estudios','users.id','=','estudios.usuario_id')
                ->join('profesionals','profesionals.id','=','estudios.profesionals_id')
                ->select('users.*','estudios.*','profesionals.*')
                ->where('users.id','=',$id)
                ->orderBy('estudios.fecha_est','desc')
                ->get();
//echo $usuarios; echo $user;
return view('/pacientes.index',compact('usuarios','user'));
            }
        //return view('home');
    }
}
