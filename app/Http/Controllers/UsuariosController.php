<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\User;
use App\Provincia;
use App\Localidade;
use App\Seguro;
use App\Role;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usuarios=DB::table('users')
        ->where('role_id','=','2')
        ->whereNull('deleted_at')
        ->get();
        
        return view("usuarios.index",compact("usuarios"));

        //return view("usuarios.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        /*$usuarios = DB::table('users')
        ->join('provincias','users.provincia_id','=','provincias.id')
        ->select('users.*','provincias.*')
        ->get();*/
        //echo ($usuarios);
        $usuarios = Provincia::all();
        $localidades= Localidade::all();
        $seguros= Seguro::all();
        $roles= Role::all();
        return view("usuarios.create",compact("usuarios","localidades","seguros","roles"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$this->validate($request, ['name'=>'required','apellido' => 'required']);

        $entrada=$request->all();

        User::create($entrada);

        return redirect('/usuarios')->with('success','Usuario agregado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $usuarios=User::findOrFail($id);
        /*$provincias=Provincia::findOrFail($id);
        $localidades=Localidade::findOrFail($id);
        $seguros=Seguro::findOrFail($id);
        $roles=Role::findOrFail($id);*/
        
        //echo ($usuarios);
        return view("usuarios.show", compact("usuarios"));//,'provincias','localidades','seguros','roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usuarios=User::findOrFail($id);
        $provincias=Provincia::all();
        $localidades=Localidade::all();
        $seguros=Seguro::all();
        $roles=Role::all();

        return view ("usuarios.edit", compact("usuarios","provincias","localidades","seguros","roles"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $usuarios=User::findOrFail($id);

        $usuarios->update($request->all());

        return redirect('/usuarios')->with('success','Paciente modificado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usuarios=User::findOrFail($id);

        $usuarios->delete();

        return redirect("/usuarios");
    }
}
