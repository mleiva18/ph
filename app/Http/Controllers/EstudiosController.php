<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Estudio;
use App\User;
use App\Profesional;
use Carbon\Carbon;

class EstudiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usuarios = DB::table('estudios')
        ->join('users','users.id','=','estudios.usuario_id')
        ->join('profesionals','profesionals.id','=','estudios.profesionals_id')
        ->select('users.*','estudios.*','profesionals.*')
        ->get();
        //echo ("$estudios");echo("$usuarios");echo("$profesionales");
        return view("estudios.index",compact("usuarios"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $usuarios= User::all();
        $profesionales= Profesional::all();
        
        return view ('estudios.create', compact('usuarios','profesionales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $entrada=$request->all();

        if($archivo=$request->file('file')){
            
            $nombre=$entrada['orden'] . ' - ' .$archivo->getClientOriginalName();

            $archivo->move('images/resultados',$nombre);

            $entrada['ruta']=$nombre;
        }

        Estudio::create($entrada);

        return redirect('/estudios')->with('success','Estudio cargado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
