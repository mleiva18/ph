<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    //

    public function afiliado(){
        return $this->hasMany("App\Afiliado", 'pronvincia_id');
    }

    protected $fillable=["NombreProvincia"];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
