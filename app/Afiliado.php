<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Afiliado extends Model
{
    //

    use SoftDeletes;

    protected $dates=['deleted_at'];

    public function provincia(){
        return $this->belongsTo("App\Provincia");
    }

    protected $fillable=["nombres","apellidos","domicilio","fecha_nacimiento","nro", "ruta"];
}
