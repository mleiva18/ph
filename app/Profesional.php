<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profesional extends Model
{
    //
    use SoftDeletes;

    protected $dates=['deleted_at'];

    protected $fillable=["NombreProf","ApellidoProf","comentarios"];

    public function estudio(){
        return $this->belongsTo(Estudio::class);
    }
}
