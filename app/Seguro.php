<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seguro extends Model
{
    //
    use SoftDeletes;

    protected $dates=['deleted_at'];

    protected $fillable=["NombreSeguro"];

    public function user(){
        
    
        return $this->belongsTo(User::class);
    }
}
