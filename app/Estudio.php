<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudio extends Model
{
    //
    protected $fillable=["fecha_est","usuario_id","profesionals_id","comentarios_est","ruta","orden"];

    public function user(){
        
    
        return $this->belongsTo(User::class);
    }

    public function profesional(){
        
    
        return $this->belongsTo(Profesional::class);
    }
}
