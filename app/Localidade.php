<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localidade extends Model
{
    //
    public function user(){
        return $this->belongsTo(User::class);
    }

    protected $fillable=["NombreLocalidad"];
}
