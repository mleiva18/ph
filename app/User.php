<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'apellido','domicilio','nro','du','provincia_id','localidade_id','tel','nroseguro','role_id','fecha','seguro_id','username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    use SoftDeletes;

    protected $dates=['deleted_at'];

    //protected $fillable=["apellido","domicilio","nro"];

    public function provincia(){
        return $this->belongsTo("App\Provincia");
    }

    public function seguro(){
        return $this->belongsTo(Seguro::class);
    }

    public function localidade(){
        return $this->belongsTo(Localidade::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function estudio(){
        return $this->belongsTo(Estudio::class);
    }
}
