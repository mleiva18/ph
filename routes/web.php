<?php

use Illuminate\Support\Facades\Route;

use App\Afiliado;
use App\Provincia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/provincia/{id}/afiliado', function($id){
    return Provincia::find($id)->afiliado;
});

Route::get('/afiliado/{id}/provincia', function($id){
    return Afiliado::find($id)->provincia->NombreProvincia;
});

Route::resource ('/afiliados','AfiliadosController');

Route::group(['middleware' => ['web']],function(){
    Route::resource('afiliados','AfiliadosController');
});

/*Route::get('/inicio','AfiliadosController@index');

Route::get('/crear','AfiliadosController@create');

Route::put('/actualizar','AfiliadosController@update');

Route::get('/insertar','AfiliadosController@store');

Route::get('/borrar','AfiliadosController@destroy');*/

Route::resource ('/usuarios','UsuariosController');

Route::group(['middleware' => ['web']],function(){
    Route::resource('usuarios','UsuariosController');
});

Route::resource ('/profesionales','ProfesionalsController');

Route::group(['middleware' => ['web']],function(){
    Route::resource('profesionals','ProfesionalsController');
});

Route::resource ('/seguros','SegurosController');

Route::group(['middleware' => ['web']],function(){
    Route::resource('seguros','SegurosController');
});

Route::resource ('/estudios','EstudiosController');

Route::group(['middleware' => ['web']],function(){
    Route::resource('estudios','EstudiosController');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Rutas de autenticación
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout', function (){Auth::logout();return redirect('/www.laboratorioph.com.ar');})->name('logout');
//Rutas de registro
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
//Rutas reset contraseña
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');