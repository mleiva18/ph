<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfiliadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afiliados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->integer('du');
            $table->string('domicilio');
            $table->integer('nro');
            $table->string('dpto');
            $table->string('piso');
            $table->string('partido');
            $table->string('localidad');
            $table->string('tel');
            $table->string('cel');
            $table->date('fecha_nacimiento');
            $table->string('sexo');
            $table->string('nacionalidad');         

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afiliados');
    }
}
