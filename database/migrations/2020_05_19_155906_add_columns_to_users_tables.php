<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('apellido');
            $table->integer('du');
            $table->string('domicilio');
            $table->string('nro');
            $table->string('tel');
            $table->date('fecha');
            $table->integer('id_roles');
            $table->integer('id_seguros');
            $table->integer('id_provincias');
            $table->integer('id_localidades');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('apellido');
            $table->dropColumn('du');
            $table->dropColumn('domicilio');
            $table->dropColumn('nro');
            $table->dropColumn('tel');
            $table->dropColumn('fecha');
            $table->dropColumn('id_roles');
            $table->dropColumn('id_seguros');
            $table->dropColumn('id_provincias');
            $table->dropColumn('id_localidades');
            $table->dropColumn('deleted_at');
        });
    }
}
